if (process.argv.length < 4) {
  console.error('Usage: node diff.js <OLD> <NEW>');
  console.error('Output keys that appear in NEW but not in OLD.');
  process.exit(1);
}

var fs = require('fs');
var Promise = require('bluebird');
var xml2js = require('xml2js');
xml2js.parseString = Promise.promisify(xml2js.parseString);

function dictify(xmlArr) {
  var ret = {};
  xmlArr.forEach((v) => {
    ret[v.$.name] = v._;
  });
  return ret;
}

var fOld = fs.readFileSync(process.argv[2], 'utf-8');
var fNew = fs.readFileSync(process.argv[3], 'utf-8');

Promise.all([
  xml2js.parseString(fOld),
  xml2js.parseString(fNew)
]).spread((xmlOld, xmlNew) => {
  var dictOld = dictify(xmlOld.resources.string);
  var dictNew = dictify(xmlNew.resources.string);

  for (var key in dictNew) {
    if (!dictOld.hasOwnProperty(key)) {
      console.log(`<string name="${key}">${dictNew[key]}</string>`);
    }
  }
});
