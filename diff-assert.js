if (process.argv.length < 4) {
  console.error('Usage: node diff-ref.js <RefA> <RefB>');
  console.error('Check whether there is any missing translations.');
  process.exit(1);
}

const {fs, Promise, xml2js, dictify} = require('./common')

function isWhitelisted(key) {
  if (key.match(/^(Telegram)?passport/i)) return true
  if (key.match(/^formatter/i)) return true
  if (key.match(/^MapPreviewProvider(Telegram|Google|Yandex)/i)) return true
  if (key.match(/^Points_\w+/i)) return true
  if ([
    'AppName',
    'AppNameBeta',
    'PaymentCardExpireDate',
    'TimeToEdit',
    'URL',
    'NotificationMessageText',
    'NotificationMessageGroupText',
    'NotificationHiddenName',
    'NotificationHiddenChatName',
    'NotificationMessagesPeopleDisplayOrder',
    'SecretChatName',
    'EncryptionKeyLink',
    'EmojiSuggestionsUrl',
    'NotificationsLed',
    'TelegramFaqUrl',
    'PrivacyPolicyUrl',
    'AuthAnotherClientDownloadClientUrl',
    'AttachGif',
    'NetworkUsageWiFiTab',
    'Page1Title',
    'VoipInCallBranding',
    'VoipInVideoCallBranding',
    'CallMessageWithDuration',
    'CallViaTelegram',
    'VideoCallViaTelegram',
    'AccDescrGIFs',
  ].indexOf(key) >= 0) return true
  return false
}

var fRefA = fs.readFileSync(process.argv[2], 'utf-8');
var fRefB = fs.readFileSync(process.argv[3], 'utf-8');

Promise.all([
  xml2js.parseString(fRefA),
  xml2js.parseString(fRefB)
]).spread((xmlRefA, xmlRefB) => {
  var dictRefA = dictify(xmlRefA.resources.string);
  var dictRefB = dictify(xmlRefB.resources.string);

  const logsAdded = [], logsRemoved = [], logsUpdated = []

  for (const [key, before] of dictRefA) {
    if (!dictRefB.has(key)) {
      logsRemoved.push({key, before})
    }
  }
  for (const [key, after] of dictRefB) {
    let a, b
    if (!dictRefA.has(key)) {
      logsAdded.push({key, after})
    } else if ((a = dictRefA.get(key)) == (b = dictRefB.get(key)) && !isWhitelisted(key)) {
      logsUpdated.push({key, before: a, after: b})
    }
  }

  ;([logsAdded, logsRemoved, logsUpdated])
  .forEach(logs => {
    // logs.sort(({key: a}, {key: b}) => a == b ? 0 : (a < b ? -1 : 1))
    if (logs.length) {
      console.log(`vvvvvv x${logs.length} ------`)
    }

    logs.forEach(({key: _key, before, after}, idx) => {
      const key = _key.padEnd(40)
      if (logs == logsAdded) {
        console.log(`+ ${key}: ${after}`)
      } else if (logs == logsRemoved) {
        console.log(`- ${key}`)
      } else if (logs == logsUpdated) {
        console.log(`* [${(idx + '').padStart(3)}]: ${key}`)
        console.log(`  --> ${before}\n`)
      }
    })
    if (logs.length) {
      console.log(`^^^^^^ x${logs.length} ------`)
      console.log('')
    }
  })
});
