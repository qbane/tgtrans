if (process.argv.length < 4) {
  console.error('Usage: node diff.js <OLD> <NEW>');
  console.error('Output keys that appear in NEW but not in OLD.');
  process.exit(1);
}

const {fs, Promise, xml2js, dictify} = require('./common')

var fOld = fs.readFileSync(process.argv[2], 'utf-8');
var fNew = fs.readFileSync(process.argv[3], 'utf-8');

Promise.all([
  xml2js.parseString(fOld),
  xml2js.parseString(fNew)
]).spread((xmlOld, xmlNew) => {
  var dictOld = dictify(xmlOld.resources.string);
  var dictNew = dictify(xmlNew.resources.string);

  for (const [key, value] of dictNew) {
    if (!dictOld.has(key)) {
      console.log(`<string name="${key}">${value}</string>`);
    }
  }
});
