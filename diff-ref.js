if (process.argv.length < 4) {
  console.error('Usage: node diff-ref.js <RefA> <RefB>');
  console.error('Compare two reference i18n file. Report differences found.');
  process.exit(1);
}

const {fs, Promise, xml2js, dictify} = require('./common')

var fRefA = fs.readFileSync(process.argv[2], 'utf-8');
var fRefB = fs.readFileSync(process.argv[3], 'utf-8');

Promise.all([
  xml2js.parseString(fRefA),
  xml2js.parseString(fRefB)
]).spread((xmlRefA, xmlRefB) => {
  var dictRefA = dictify(xmlRefA.resources.string);
  var dictRefB = dictify(xmlRefB.resources.string);

  const logsAdded = [], logsRemoved = [], logsUpdated = []

  for (const [key, before] of dictRefA) {
    if (!dictRefB.has(key)) {
      logsRemoved.push({key, before})
    }
  }
  for (const [key, after] of dictRefB) {
    let a, b
    if (!dictRefA.has(key)) {
      logsAdded.push({key, after})
    } else if ((a = dictRefA.get(key)) != (b = dictRefB.get(key))) {
      logsUpdated.push({key, before: a, after: b})
    }
  }

  ;([logsAdded, logsRemoved, logsUpdated])
  .forEach(logs => {
    logs.sort(({key: a}, {key: b}) => a == b ? 0 : (a < b ? -1 : 1))
    logs.forEach(({key: _key, before, after}) => {
      const key = _key.padEnd(40)
      if (logs == logsAdded) {
        console.log(`+ ${key}: ${after}`)
      } else if (logs == logsRemoved) {
        console.log(`- ${key}`)
      } else if (logs == logsUpdated) {
        console.log(`* ${key}`)
        console.log(`  BEFORE: ${before}`)
        console.log(`  AFTER : ${after}`)
      }
    })
    console.log('')
  })
});
