#!/usr/bin/env python3

import re
import lxml.etree as ElementTree


PARSER = ElementTree.XMLParser(strip_cdata=False)

def read_src(filename):
    tree = ElementTree.parse(filename, parser=PARSER)
    root = tree.getroot()

    idx = 0
    lst = []
    name2idx = {}
    for el in root:
        if el.tag == ElementTree.Comment:
            # curCategory = el.text
            continue
        else:
            lst.append(el)
            name2idx[el.get('name')] = idx
            idx += 1

    return root, lst, name2idx

def write(root, f):
    s = ElementTree.tostring(root, encoding='unicode')

    s = re.sub(r'[\u200d\u2642\ufe0f\U00010000-\U0010ffff]',
               lambda x: f'&#{ord(x.group(0))};',
               s)

    f.write('<?xml version="1.0" encoding="utf-8"?>\n')
    f.write(s)

tree_ref, lst_ref, n2i_ref = read_src('ref-6.1.1.xml')
tree_tar, lst_tar, n2i_tar = read_src('AndroidTW-6.1.1-20200511.xml')

idx_ref = 0
idx_tar = 0

len_ref = len(lst_ref)
len_tar = len(lst_tar)


while idx_ref < len_ref and idx_tar < len_tar:
    el1 = lst_ref[idx_ref]
    el2 = lst_tar[idx_tar]
    k1 = el1.get('name')
    k2 = el2.get('name')

    if k1 != k2:
        assert k1 in n2i_ref
        assert k2 in n2i_tar

        resolved = False
        el2_pos = tree_tar.index(el2)

        if k2 not in n2i_ref:
            # tar to be deleted
            print('Deleting unused key "{}": {}'.format(k2, el2.text))
            tree_tar.remove(el2)
            resolved = True
            idx_tar += 1

        if k1 not in n2i_tar:
            # ref to be added
            # FIXME: add ignoring comments, manual fix needed
            tree_tar.insert(el2_pos, el1)
            resolved = True
            idx_ref += 1

        if not resolved:
            # both have these keys; skip
            # print(f'Conflict at ref={idx_ref} [{k1}], tar={idx_tar} [{k2}]')
            idx_tar += 1
            idx_ref += 1
    else:
        idx_tar += 1
        idx_ref += 1


with open('test-out.xml', 'w') as f:
    write(tree_tar, f)
