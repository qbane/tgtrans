const Promise = require('bluebird');

var xml2js = require('xml2js');
xml2js.parseString = Promise.promisify(xml2js.parseString);

function dictify(xmlArr) {
  var ret = new Map();
  xmlArr.forEach(v => {
    ret.set(v.$.name, v._);
  });
  return ret;
}

module.exports = {
  fs: require('fs'),
  Promise,
  xml2js,
  dictify,
}
