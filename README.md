# My Telegram Translation

Telegram 臺灣正體中文計畫，始自 2016

https://telegra.ph/how-to-apply-translation-11-05

The localization is reviewed by diffing against the reference translation (`ref-*`) copied from [the repo of the official Android app](https://github.com/DrKLO/Telegram/blob/master/TMessagesProj/src/main/res/values/strings.xml). Old translations are compared with ripped files from the APK whose names are prefixed with, well, `rip-*`.
